<?php
include ("config.php");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Wunschbox</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/css/mdb.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Shrikhand" rel="stylesheet">
    <style media="screen">
      h1, h2, h3 {
        font-family: 'Shrikhand', cursive;
        text-transform: uppercase;
      }
      body, html {
        font-family: 'Montserrat', sans-serif;
        text-transform: uppercase;
      }
    </style>
  </head>
  <body>
    <?php
      if (isset($_POST["Name"]) &&
          isset($_POST["Interpret"]) &&
          isset($_POST["Titel"]) &&
          isset($_POST["Gruss"])) {
        if ($db) {
          $sql = "INSERT INTO wunschbox (name, interpret, titel, gruss) VALUES (?,?,?,?)";
          $kommando = mysqli_prepare($db, $sql);
          mysqli_stmt_bind_param($kommando, "ssss",
          $_POST["Name"],
          $_POST["Interpret"],
          $_POST["Titel"],
          $_POST["Gruss"]);
          if (mysqli_stmt_execute($kommando)) {
            $id = mysqli_insert_id($db);
            echo "Ihr Wunsch wurde eingesendet. Der Sendende Moderator wird ihn lesen und danach entscheiden was damit geschieht.";
          } else {
            echo "Fehler: " . mysqli_error($db) . "!";
          }
          mysqli_close($db);
        } else {
          echo "Fehler: " . mysqli_connect_error() . "!";
        }
      }
    ?>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 style="text-align:center; font-weight: 400">Wunschbox</h2>
          <form class="" method="post">
            <div class="md-form">
              <input type="text" id="Name" name="Name" class="form-control">
              <label for="Name">Name</label>
            </div>
            <div class="md-form">
              <input type="text" id="Interpret" name="Interpret" class="form-control">
              <label for="Interpret">Interpret</label>
            </div>
            <div class="md-form">
              <input type="text" id="Titel" name="Titel" class="form-control">
              <label for="Titel">Interpret</label>
            </div>
            <div class="md-form">
              <textarea type="Gruss" id="Gruss" class="form-control md-textarea" rows="3"></textarea>
              <label for="Gruss">Dein Gruss</label>
            </div>
            <div class="text-center mt-4">
                <button class="btn btn-outline-secondary" name="Submit" type="submit">Send<i class="fas fa-paper-plane ml-2"></i></button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="container-fluid">
        <center>&copy; <a href="https://fq-programming.xyz">FQ-Programming</a></center>
    </div>
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/js/mdb.min.js"></script>
  </body>
</html>
