-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 08, 2018 at 05:20 PM
-- Server version: 10.0.34-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wunschbox`
--

-- --------------------------------------------------------

--
-- Table structure for table `wunschbox`
--

CREATE TABLE `wunschbox` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `interpret` varchar(255) NOT NULL,
  `titel` varchar(255) NOT NULL,
  `gruss` text NOT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gespielt` set('NEIN','JA') NOT NULL DEFAULT 'NEIN'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wunschbox`
--

INSERT INTO `wunschbox` (`id`, `name`, `interpret`, `titel`, `gruss`, `datum`, `gespielt`) VALUES
(1, 'John Doe', 'U96', 'Das Boot', 'Keinen', '2018-07-08 11:13:43', 'NEIN'),
(3, 'Max Mustermann', 'Scooter', 'Friends', 'Keinen', '2018-07-08 12:27:16', 'JA');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wunschbox`
--
ALTER TABLE `wunschbox`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wunschbox`
--
ALTER TABLE `wunschbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
