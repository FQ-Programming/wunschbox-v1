<?php
include ("config.php");

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Wunschbox Adminarea</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/css/mdb.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Shrikhand" rel="stylesheet">
    <style media="screen">
      h1, h2, h3 {
        font-family: 'Shrikhand', cursive;
        text-transform: uppercase;
      }
      body, html {
        font-family: 'Montserrat', sans-serif;
        text-transform: uppercase;
      }
    </style>
  </head>
  <body>
    <?php
      if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
        if (isset($_GET["ok"])) {
          if ($db) {
            $id = mysqli_escape_string($db, $_GET["id"]);
            $sql = "DELETE FROM wunschbox WHERE id=$id";
            if (mysqli_query($db, $sql)) {
              echo "<p> Eintrag erfolgreich gel&ouml;scht. </p>
              <p><a href=\"admin.php\">Zur&uuml;ck zur &uuml;bersicht</a></p>";
            } else {
              echo "Fehler: " . mysqli_error($db);
            }
            mysqli_close($db);
          } else {
            echo "Fehler: " . mysqli_connect_error($db);
          }
        } else {
          printf("<a href=\"admin.php?id=%s&ok=1\">Wirklich l&ouml;schen?</a>", urlencode($_GET["id"]));
        }
      } else { ?>
        <table width="100%" style="width:100%">
          <tr width="100%" style="width:100%">
            <td style="width:5%;">ID</td>
            <td style="width:15%;">Datum</td>
            <td style="width:15%;">Name</td>
            <td style="width:20%;">Interpret</td>
            <td style="width:20%;">Titel</td>
            <td style="width:15%;">Gruss</td>
            <td style="width:10%">Action</td>
          </tr>
          </table>

        <?php
          if ($db) {
            $sql = "SELECT * FROM wunschbox ORDER BY datum DESC";
            $ergebnis = mysqli_query($db, $sql);
            while ($zeile = mysqli_fetch_object($ergebnis)) {
              $id = $zeile->id;
              ?>

              <table width="100%" style="width:100%">
              <tr width="100%" style="width:100%">
                <td style="width:5%;"><?=$zeile->id;?></td>
                <td style="width:15%;"><?=$zeile->datum;?></td>
                <td style="width:15%;"><?=$zeile->name;?></td>
                <td style="width:20%;"><?=$zeile->interpret;?></td>
                <td style="width:20%;"><?=$zeile->titel;?></td>
                <td style="width:15%;"><?=$zeile->gruss;?></td>
                <td style="width:10%"><form action="admin.php?id=<?php echo $id; ?>" method="post">
    							<div class="form-group">
    								<h5>DELETE</h5>
    							</div>
                  <div class="form-group">
    								<button type="submit" class="btn btn-block btn-success"><i class="fas fa-check"></i>ja</button>
    							</div>
    						</form>
                <p>Wurde gespielt?</p>
                <?=$zeile->gespielt; ?>
              </td>
              </tr>
              </table>
              <hr>
              <?php
            }
            mysqli_close($db);
          }else {
            echo "Fehler: " . mysqli_connect_error() . "!";
          }

          }


    ?>
    <div class="container-fluid">
        <center>&copy; <a href="https://fq-programming.xyz">FQ-Programming</a></center>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/js/mdb.min.js"></script>

  </body>
</html>
